module.exports = {
    title: 'GitLab ❤️ VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/',
    dest: 'public',
    themeConfig: {
        search: true,
        searchMaxSuggestions: 10,
        nav: [
          { text: 'Home', link: '/' },
          { text: 'About', link: '/about/about.html' },
          { text: 'External', link: 'https://google.com' },
        ]
      }
}