---
home: true
heroImage: /hero.png
features:
- title: Test
  details: Minimal setup with markdown-centered project structure helps you focus on writing.
- title: VTest
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
- title: PeTest
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
footer: MIT Licensed | Copyright © 2018-present Evan You
---

## about